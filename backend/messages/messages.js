/* eslint-disable linebreak-style */
const messages = {registeredSuccess: { message: 'You are registered successfully' },
    alreadyRegisteredUser: { message: 'Already registered' },
    loginSuccess: { message: 'You are logged in successfully' },
    loginEmailNotRegistered: { message: 'Email not registered' },
    wrongPass: { message: 'Password does not match' },
    emailReq: { message: 'Email id is required' },
    passReq: { message: 'Password is required' },
    fistNameReq: { message: 'FirstName required' },
    lastNameReq: { message: 'LastName required' },
    updatedProfile: { message: 'Profile successfully updated' },
    process: { message: 'you can edit now' },
    invalidCredentials: { message: 'Invalid credentials' },
    doNotChangeEmail: { message: 'Cannot change email and password' },
    changePass: { message: 'Password changed successfully' },
    orderSuccess: { message: 'Order placed successfully' },
    unAuthorized: { message: 'You are unauthorized!' },
    alreadyLoggedIn: { message: 'Already logged in user' },
    productRegistered: { message: 'Already Registered Product' },
    routeNotFound: { message: 'Route Not Found!' },
   
    registerSuccess: { message: 'Registered successfully' },
   
   
    pleaseAddText: { message: 'Please add text ' },
    titleRequired:{message:'Title is required'},
    descriptionRequired:{message:'Description is required'},
    blogNotFound:{message:'There is no blog Present with specified Id'},
    userNamePattern:{message:'The pattern for username should be: user_123 or user.124'},
    emailPattern:{message:'Please enter a valid email like user123@gmail.com'},
    validMobile:{message:'Please enter valid mobile with country code'},
    validGender:{message:'Please enter a valid gender, the options are 1.Male 2.Female 3.Others'},
    mandatoryFields:{message:'Please add all fields'},
    duplicateTitle:{message:'You cannot use same title please change title name'},
    userNotFound:{message:'User not found'},
    bothIdDiff:{message:'The id you requested in params and id of email you provided are not same'},
    error:{message:'Error!'}
};

module.exports = messages; 
