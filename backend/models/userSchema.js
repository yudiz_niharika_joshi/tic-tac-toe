const mongoose = require('mongoose');
const userSchema = mongoose.Schema(
    {
        sFirstName: {
            type: String,
            required: [true, 'Please add a first name'],
        },
        sLastName: {
            type: String,
            required: [true, 'Please add a last name'],
        },

        sEmail: {
            type: String,
            required: [true, 'Please add an email'],
            unique: true,
        },
        sPassword: {
            type: String,
            required: [true, 'Please add a password'],
        },

        sUserName: {
            type: String,
            required: [true, 'Please add a username'],
            unique: true,
        },

        nMobile: {
            type: String,
            required: [true, 'Please add a number'],
        },
        sGender: {
            type: String,
            required: [true, 'Please add a gender'],
        },
        aToken: {
            type: Array,
        },
        sDateOfBirth: {
            type: Date,
            required: [true, 'Please add date of birth'],
        },
    },
    { timestamps: true });
const Users = mongoose.model('User', userSchema);
module.exports = Users;
