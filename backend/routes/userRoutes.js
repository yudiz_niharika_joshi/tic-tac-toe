const express = require('express');
const router = express.Router();


const { singUp, logIn,editProfile,forgotPassword,resetPassword } =
require('../controller/userControllers');
const middleware=require('../middlewares/middlewares');




router.post('/signup', singUp);
router.post('/login', logIn);
router.put('/editProfile/:id',editProfile);
// router.get('/me',protect,getLoggedinUser)
// /* New */
// router.post('/logout',logoutUser)/* New */

router.post('/forgotPassword',forgotPassword);/* New */

// router.post('/forgotPassword',forgotPassword)/* New */
router.put('/editprofile/:id', editProfile);

// router.put('/changePassword/:id',changePassword)/* New */
router.post('/resetPassword/:token',middleware.authorizationToken,resetPassword);/* New */
// router.delete('/:id',deleteUser)
module.exports = router;
