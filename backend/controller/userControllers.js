const { generateToken, forgotPasswordToken,verifyToken}=require('../helper/jwt');
const{sendMail}=require('../helper/sendMail');

const { comparePassword, hashPassword } = require('../helper/bcryptPassword');

const Users = require('../models/userSchema');
const messages=require('../messages/index');



class Controller {
    async singUp(req, res) {
        const { sFirstName, sLastName, sUserName, sEmail, sPassword, nMobile, sGender, sDateOfBirth } = req.body;
        const hashedPass = await hashPassword(sPassword);

        const userData = {
            sFirstName,
            sLastName,
            sUserName,
            sEmail,
            sPassword: hashedPass,
            nMobile,
            sGender,
            sDateOfBirth,};
        try {
            if (await Users.findOne({ sEmail })) return res.status(400).json({ message: 'Email already register' });
            const user = new Users(userData);
            const token = await generateToken(user._id);
            user.aToken.push(token);
            const response = await user.save();
            return res.status(201).json({ message: 'Successfully register', response, token });} catch (error) {
            return res.status(500).json({ message: 'Error' + error.message });}}

    async logIn(req, res) {
        const { sEmail, sPassword } = req.body;
        try {
            const user = await Users.findOne({ sEmail });
            if (!user) return res.status(400).json({ message: 'User not exits' });
            const isMatch = await comparePassword(sPassword, user.sPassword);
            if (!isMatch) return res.status(400).json({ message: 'password not match' });
            const token = await generateToken(user._id);
            if (user.aToken.length > 4) user.aToken.shift();
            user.aToken.push({ token });
            await user.save();
            return res.status(200).json({ message: 'Successfully login', token });
        } catch (error) {
            return res.status(500).json({ message: 'Error' + error.message });}}
    
    async editProfile(req,res){

        try{
            const id=req.params.id;
            const update=await Users.updateOne({_id:id},{
        
                sFirstName:req.body.sFirstName,
                sLastName:req.body.sLastName,
                sUserName:req.body.sLastName,
                nMobile:req.body.sLastName,
                sDateOfBirth:req.body.sDateOfBirth},{returnOriginal:false}).exec();
            const user=await Users.findOne({id});
            console.log(user);
            console.log(update);

            return res.status(messages.status.badrequest).json(messages.messages.updatedProfile);}catch(error){return  res.status(500).json({ message: 'Error' + error.message });}    
    }
    async forgotPassword(req,res){
        try {
            const{sEmail}=req.body;

            const user = await Users.findOne({ sEmail });
            if (!user) return res.status(404).send({ message: 'User not found' });
            const token = await forgotPasswordToken(user._id);
            const link = `${req.protocol}://${req.get('host')}/user/resetpassword/${token}`;
            // eslint-disable-next-line no-unused-vars
            const response = await sendMail(sEmail, link);
            return res.status(200).send({ message: 'Password reset link has been successfully sent to your inbox' });
        } catch (error) {
            return res.status(500).json({ message: 'Error :' + error.message });}

    }
    async changePassword(req,res){
        const { sCurrentPassword, sNewPassword } = req.body;
        const { _id, sPassword } = req.user;
        try {
            const isMatch = await comparePassword(sCurrentPassword, sPassword);
            if (!isMatch) return res.status(400).json({ message: 'password not match' });
            const hashedPass = await hashPassword(sNewPassword);
            await Users.updateOne({ _id }, { sPassword: hashedPass });
            return res.status(200).json({ message: 'password changed' });
        } catch (error) {
            return res.status(500).json({ message: 'Error :' + error.message });}


    }
    async resetPassword(req,res){
        const { sPassword } = req.body;
        if (!sPassword) return res.status(400).json({ message: 'please enter password' });
        const { token } = req.params;
        try {
            const decoded = verifyToken(token);
            const hashedPass = await hashPassword(sPassword);
            const response = await Users.updateOne({ _id: decoded.id }, { sPassword: hashedPass });
            if (!response.modifiedCount) return res.status(400).json({ message: 'something went wrong' });
            return res.status(200).json({ message: 'password changed' });
        } catch (error) {
            return res.status(500).json({ message: 'Error :' + error.message });
        }

    }
}
module.exports = new Controller();
