/* eslint-disable no-undef */
require('dotenv').config();
const nodemailer=require('nodemailer');
const sendMail= (recieverEmail,link)=>{
    
       
    const transport=nodemailer.createTransport(
        {
            service:'gmail',
            auth:{
                user:process.env.ADMIN_MAIL,
                pass:process.env.ADMIN_PASSWORD
            }});

    const mailOptions={
        from:process.env.ADMIN_MAIL,
        to:recieverEmail,
        subject:'Forgot Password',
        text:'This is a link to reset password \n '+ link};

    transport.sendMail(mailOptions,function(error,info){
        if(error) console.log(error);
        console.log('email sent'+info.response);});

    console.log('hi');
    // return res.status(messages.status.statusSuccess).json({message:'Mail Sent'});
};


module.exports={sendMail};