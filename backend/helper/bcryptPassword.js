const bcrypt = require('bcryptjs');

const hashPassword = (password) => {
    const hashedPass = bcrypt.hashSync(password, 8);
    return hashedPass;
};

const comparePassword = (password, passHash) => {
    const comparedPassword = bcrypt.compareSync(password, passHash);
    return comparedPassword;
};

module.exports = {hashPassword,comparePassword,
};
