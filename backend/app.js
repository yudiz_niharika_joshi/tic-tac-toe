/* eslint-disable no-undef */
const express = require('express');
const cors = require('cors');
const connectDB = require('./db/dbConfig');
require('dotenv').config();
connectDB();
const app = express();
app.use(express.json());
app.use(cors('*'));
app.use(express.urlencoded({ extended: false }));
app.use('/api/users', require('./routes/userRoutes'));

app.listen(process.env.PORT || 8000, () => {
    console.log('Server started on port ' + process.env.PORT);
});
