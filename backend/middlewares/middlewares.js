require('dotenv').config();
const jwt = require('jsonwebtoken');
const middleware = {};
const Users = require('../models/userSchema');

middleware.authorizationToken = (req, res, next) => {
    const token = req.headers?.authorization;
    if (!token) return res.status(401).json({ message: 'error' });
    // eslint-disable-next-line no-undef
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, async (err, data) => {
        if (err) return res.status(401).json({ message: err });
        const user = await Users.findOne({
            _id: data.id,
            'aTokens.token': token,
        });
        if (!user) return res.status(401).json({ message: 'Something went wrong , please login again' });
        req.user = user;
        req.token = token;
        next();});};

middleware.isAdmin = (req, res, next) => {
    if (!req.user.aRole.includes('admin')) return res.status(401).json({ message: 'unauthorize' });
    next();
};

module.exports = middleware;
